import Entrance from "../pages/Entrance";
import Feed from "../pages/Feed";

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const AppNavigator = createStackNavigator(
  {
    Home: Entrance,
    Feed: Feed
  },
  {
    initialRouteName: "Home"
  }
);



function AppContainer() {
  return (
	<NavigationContainer style={styles.container}>
        <Stack.Navigator style={styles.screen}>
        <Stack.Screen name="Home" component={Entrance} style={styles.screen}/>
        <Stack.Screen name="Feed" component={Feed} style={styles.screen}/>
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppContainer;