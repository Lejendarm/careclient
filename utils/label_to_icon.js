
const icon_translations = {
    "info": {
      name: 'info',
      type: 'MaterialIcons',
      style: {color: '#019688'}
    },
    "help": {
      name: 'healing',
      type: 'MaterialIcons',
      style: {color: '#ff5528'}
    },
    "social": {
      name: 'people',
      style: {color: '#00bcd6'}
    }
};

export function label_to_icon(t) {
  return icon_translations[t];
}
