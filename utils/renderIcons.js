import React from "react";
import { label_to_icon } from "./label_to_icon";
import { View, Image, Text, TextInput } from "react-native";
import { Button, Icon, Input, CheckBox } from 'react-native-elements';

export function renderIcons(tags) {
	let tagsFormated = tags.map(
		(tag) => <Icon 	name={label_to_icon(tag).name} 
						color={label_to_icon(tag).style.color} 
						type={label_to_icon(tag).type}
						iconStyle={{marginLeft: 10}}
						/>
	);

	return 	<View style={{flex: 1, flexDirection: 'row-reverse'}}>
				{tagsFormated}
			</View>;
}