import React from "react";
import axios from 'axios';
import env from "../config/env";

class ApiService {

	static currentInstance = null;
	token = null;
	config = {};

	constructor() {
		let current_env = process.env.NODE_ENV || "prod"; 
		// this.config = {apiUrl: 'https://care-x02.appspot.com/'};
		this.config = {apiUrl: 'http://192.168.1.9:8080'};
	}

	static instance() {
		if (ApiService.currentInstance == null) {
			ApiService.currentInstance = new ApiService();
		}
		return this.currentInstance;
	}

	setToken(res) {
		this.token = res.headers['set-cookie'][0].replace('token=','').replace('; Path=/; HttpOnly', '');
	}

	getHeaders() {
		if (this.token){
			return { headers: { authorization: this.token } };
		}
		return {};
	}

	post(url, body) {
		console.log(url, body, this.config);
		return axios.post(this.config['apiUrl'] + url + "?key=AIzaSyCZ1oy2sQWKOwCzgDT5LOXkUvJOtgJ8qC4", body, this.getHeaders());
	}

	put(url, body) {
		return axios.put(this.config['apiUrl'] + url + "?key=AIzaSyCZ1oy2sQWKOwCzgDT5LOXkUvJOtgJ8qC4", body, this.getHeaders());
	}


	get(url) {
		return axios.get(this.config['apiUrl'] + url + "?key=AIzaSyCZ1oy2sQWKOwCzgDT5LOXkUvJOtgJ8qC4", this.getHeaders());
	}

	delete(url) {

		return axios.delete(this.config['apiUrl'] + url + "?key=AIzaSyCZ1oy2sQWKOwCzgDT5LOXkUvJOtgJ8qC4", this.getHeaders());
	}


}

export default ApiService;