import * as Localization from "expo-localization";
import i18n from "i18n-js";

i18n.translations = {
  en: {
    EnterPhone: "Enter PhoneNumber",
    EnterFB: "Enter Facebook",
    Welcome: "Welcome back",
    Chat: "Chat",
    Shop: "Shop",
    Join: "Join",
    Login: "Login",
    Logout: "Logout",
    Content: "Content",
    HideModal: "Hide",
    "Sign in": "Sign in",
    "Log in" : "Log in"
  }
};
// Set the locale once at the beginning of your app.
// i18n.locale = Localization.locale;
i18n.locale = "en";
i18n.fallbacks = true;

export function translated(t) {
  return i18n.t(t);
}
