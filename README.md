#./config/env.ts 
```
const Env = {
  dev: {
    apiUrl: localhost
  },
  staging: {
    apiUrl: "[your.staging.api.here]",
  },
  prod: {
    apiUrl: "[your.production.api.here]",
  }
};

export default Env;
```

________________________

# Install & Run
If you are using NPM:
```
npm install
npm start
```

# Expo
expo build:ios --release-channel staging
expo build:android --release-channel staging
expo publish --release-channel staging

