import React, { Component } from 'react';
import { TouchableOpacity, Animated, StyleSheet, KeyboardAvoidingView, Image, Text } from "react-native";
import {  FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { TextInput } from 'react-native-gesture-handler';
import ApiService from "../utils/ApiService";


const api = ApiService.instance();

export default class Register extends Component {

	constructor(props) {
		super(props);
	}


  	state = {
      firstname:'', Lastname: '', email : '', password: ''
    };

  _signin = () => {
  	if (!this.state.firstname || !this.state.email |- !this.state.password)
  		return;

	api.post('/user/register', {...this.state})
		 .then(res => {
		 	console.log("Success", res);
			this.props.navigation.navigate('Authenticate');
		 })
		 .catch(function (error) {
		 	console.log("error", error);
		})
  }
  render() {
    return (
    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
    	<Text style={styles.header}>Sign in</Text>
    	<TextInput onChangeText={(firstname) => this.setState({firstname: firstname})} value={this.state.firstname} style={styles.input} placeholder="Firstname"></TextInput>
    	<TextInput onChangeText={(lastname) => this.setState({lastname: lastname})} value={this.state.lastname} style={styles.input} placeholder="Lastname"></TextInput>
    	<TextInput onChangeText={(email) => this.setState({email: email})} value={this.state.email} style={styles.input} placeholder="Email"></TextInput>
    	<TextInput type="password" secureTextEntry={true} onChangeText={(password) => this.setState({password: password})} value={this.state.password} style={styles.input} placeholder="password"></TextInput>
    	<TouchableOpacity style={styles.btnEnter} onPress={this._signin}>
    		<Text style={styles.btnEnterText}>SUBMIT</Text>
    	</TouchableOpacity>
     </KeyboardAvoidingView>
    );
  }
}

// 	{ this.state.attempt && this.state.firstname == '' && <Text>Incorrect firstname</Text>}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	},
	header: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
		color: '#428AF8'
	},
	input: {
		margin: 15,
		height: 40,
		padding: 5,
		fontSize: 16,
		borderBottomWidth: 1,
		borderBottomColor: '#428AF8'
	},
	btnEnter: {
		justifyContent: 'center',
		flexDirection: 'row',
		backgroundColor: '#428AF8',
		marginLeft: 15,
		marginRight: 15,
		padding: 10
	},
	btnEnterText: {
		color: '#ffffff',
		fontWeight: '700'
	}
})