import React from "react";
import { Animated, StyleSheet, View, Image, Text, TextInput } from "react-native";
import MaterialButtonDark from "../components/MaterialButtonDark";
import { Button, ListItem, Divider, Icon, Input, CheckBox } from 'react-native-elements';
import { translated } from "../utils/localization";
import { label_to_icon } from "../utils/label_to_icon";
import { SafeAreaView, FlatList } from 'react-native';
// import axios from 'axios';
import ApiService from "../utils/ApiService";

import FeedPost from './FeedPost';
import FeedItem from './FeedItem';
import Constants from 'expo-constants';


const api = ApiService.instance();

class Feed extends React.Component {

	state = {
		data: []
	}

	refresh() {
		api.get('/post')
			 .then(res => {
			 	console.log("Success in Feed refresh", res);
				this.setState({data: res.data});
			 })
			 .catch(function (error) {
			 	console.log("Error in Feed refresh", error);
			});
	}

	refreshItem(item) {
		api.get('/post/' + item._id)
			 .then(res => {
			 	console.log("Success in Feed Item refresh", res);
			 	let index = this.state.data.findIndex(element => element._id == res._id);

			 	let data = this.state.data;
			 	data[index] = res;
				this.setState({data: data});
			 })
			 .catch(function (error) {
			 	console.log("Error in Feed refresh", error);
			});
	}


	componentDidMount() {
    	this.refresh();
    }
 

	render() {
	return (
		<SafeAreaView style={styles.container}>
		<FeedPost  callRefresh={() => this.refresh()} />
		<FlatList
		data={this.state.data}
		renderItem={({ item }) => 
			<FeedItem 
				key={item._id} 
				id={item._id}
				refreshItem={() => this.refreshItem(item)}
				data={item} title={item.title} 
				content={item.content} author={item.author} 
				comment_nb={item.comment_nb} date={ item.date } 
				avatar_url={item.avatar_url} tags={item.tags}
			/>}
		keyExtractor={item => item._id}
		/>
		</SafeAreaView>);
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: Constants.statusBarHeight,
	},
	checkbox: {
		borderWidth: 0,
		backgroundColor: '#ffffff',
		padding: 0
	},
	item: {
		backgroundColor: '#ffffff',
		padding: 10,
		marginVertical: 8,
		marginHorizontal: 16,
		borderColor: '#606770',
		borderWidth: 0.4,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
	},
	title: {
		fontSize: 32,
	},
	comments: {
		textAlign: 'right',
		color: '#606770',
		marginTop: 6,
		paddingBottom: 10
	}
});

export default Feed;
