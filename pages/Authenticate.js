import React, { Component } from 'react';
import { TouchableOpacity, Animated, StyleSheet, View, Image, Text } from "react-native";
import {  FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { TextInput } from 'react-native-gesture-handler';
import ApiService from "../utils/ApiService";


const api = ApiService.instance();

export default class authenticate extends Component {

	constructor(props) {
		super(props);
	}


  state = {
      email : '', password: ''
    };

  _signin = () => {
  	if (!this.state.email || !this.state.password)
  		return;
	api.post('/user/authenticate', {...this.state})
		 .then(res => {
		 	console.log("Success", res);
			api.setToken(res);
			this.props.navigation.navigate('Feed');
		 })
		 .catch(function (error) {
		 	console.log("Error", error);
		 	console.log(error);
		})
  }
  render() {
    return (
    <View style={styles.container}>
    	<Text style={styles.header}>Login</Text>
    	<TextInput onChangeText={(email) => this.setState({email: email})} value={this.state.email} style={styles.input} placeholder="Email"></TextInput>
    	<TextInput type="password" secureTextEntry={true} onChangeText={(password) => this.setState({password: password})} value={this.state.password} style={styles.input} placeholder="password"></TextInput>
    	<TouchableOpacity style={styles.btnEnter} onPress={this._signin}>
    		<Text style={styles.btnEnterText}>SUBMIT</Text>
    	</TouchableOpacity>
     </View>
    );
  }
}

// 	{ this.state.attempt && this.state.firstname == '' && <Text>Incorrect firstname</Text>}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	},
	header: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
		color: '#428AF8'
	},
	input: {
		margin: 15,
		height: 40,
		padding: 5,
		fontSize: 16,
		borderBottomWidth: 1,
		borderBottomColor: '#428AF8'
	},
	btnEnter: {
		justifyContent: 'center',
		flexDirection: 'row',
		backgroundColor: '#428AF8',
		marginLeft: 15,
		marginRight: 15,
		padding: 10
	},
	btnEnterText: {
		color: '#ffffff',
		fontWeight: '700'
	}
})