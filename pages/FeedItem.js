import React from "react";
import { Animated, StyleSheet, View, Image, Text, TextInput } from "react-native";
import { Button, ListItem, Icon, Input, CheckBox } from 'react-native-elements';

import ApiService from "../utils/ApiService";
import { renderIcons } from "../utils/renderIcons";

import Comments from './Comments';
import CommentPost from "./CommentPost";

import Constants from 'expo-constants';


const api = ApiService.instance();

class FeedItem extends React.Component {

	state = {};

	constructor(props) {
		super(props);

		this.state = {
				id: this.props.data._id,
				avatar_url: this.props.data.avatar_url,
				tags: this.props.data.tags,
				comments: this.props.data.comments,
				author: this.props.data.author,
				date: this.props.data.date,
				content: this.props.data.content,
				comment_nb: this.props.comments ? this.props.comments.length : 0
		};
	}
	
	publishComment(content) {
		console.log("Publich Comment in Feed item", content);
		// api.post('/post/' + this.props.data._id + '/comments', content)
		//  .then(res => {
		//  	this.refreshItem();
		//  })
		//  .catch(function (error) {
		//  	console.log("Error in Feed refresh", error);
		// });
	}

	render() {
		return (
			<View style={styles.item} key={this.state.id}>
			<ListItem key={this.state.id}
				leftAvatar={{ source: { uri: this.state.avatar_url } }}
				rightElement={renderIcons(this.state.tags)}
				title={this.state.author}
				subtitle={this.state.date}
			/>
			<Text style={{ marginBottom: 5 }}>{this.state.content}</Text>
			<Text style={styles.comments}>{this.state.comment_nb | 0} comments</Text>
			<Comments comments={this.state.comments || []} />
			<CommentPost publishComment={(content) => {
				return this.publishComment(content)}}/>
			</View>
			);
	}
}


const styles = StyleSheet.create({
	item: {
		backgroundColor: '#ffffff',
		padding: 10,
		marginVertical: 8,
		marginHorizontal: 16,
		borderColor: '#606770',
		borderWidth: 0.4,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
	},
	comments: {
		textAlign: 'right',
		color: '#606770',
		marginTop: 6,
		paddingBottom: 10
	}
});

export default FeedItem;
