import React from "react";
import { Animated, StyleSheet, View, Image, Text, TextInput } from "react-native";
import MaterialButtonDark from "../components/MaterialButtonDark";
import { Button, ListItem, Divider, Icon, Input, CheckBox } from 'react-native-elements';
import { translated } from "../utils/localization";
import { label_to_icon } from "../utils/label_to_icon";
import { SafeAreaView, FlatList } from 'react-native';
import ApiService from "../utils/ApiService";

const api = ApiService.instance();

class FeedPost extends React.Component {
	state = {
		info: false,
		help: false,
		social: false,
		display_bar: false,
		height: new Animated.Value(0),
		opacity: new Animated.Value(0),
		text: ''
	};

submit() {

	if (this.state.text == '')
		return;

	let tags = [];
	if (this.state.info) {
		tags.push('info');
	}
	if (this.state.help) {
		tags.push('help');
	}
	if (this.state.social) {
		tags.push('social');
	}

	let req = { tags: tags, content: this.state.text}
	
	api.post('/post', req)
		 .then(res => {
		 	console.log("Success", res);
			this.hideBar();
			this.setState({text: ''});
			this.props.callRefresh();
		 })
		 .catch(function (error) {
		 	console.log("error");
			console.log(error);
		})
}

displayBar() {
	this.setState({display_bar: true}, () => {
	Animated.timing(          // Animate over time
	      this.state.height, // The animated value to drive
	      {
	        toValue: 50,           // Animate to opacity: 1 (opaque)
	        duration: 500,       // 2000ms
	    }
	    ).start(() => this.setState({opacity: 1}))
	});
}

hideBar() {
	this.setState({display_bar: false}, () => {
		this.setState({opacity: 0});
		Animated.timing(          // Animate over time
	      this.state.height, // The animated value to drive
	      {
	        toValue: 0,           // Animate to opacity: 1 (opaque)
	        duration: 500,       // 2000ms
	    }
	    ).start();
	});
}


render() {
	return (
		<View style={{...styles.item, marginBottom: 0, paddingBottom: 0}}>
			<ListItem key="post_form"	style={{marginBottom: 0, paddingBottom: 0}}
			leftAvatar={{ source: { uri: 'https://p7.hiclipart.com/preview/858/581/271/user-computer-icons-system-chinese-wind-title-column.jpg' } }}
			title={
					<TextInput placeholder={"What do you want to share ?"} 
						multiline={true}
						value={this.state.text}
						onChange={({ nativeEvent: {eventCount, target, text}}) => {
									this.setState({text: text});
									if (text.length > 0 && this.state.display_bar == false) {
										this.displayBar();
									}
									else if (text.length == 0 && this.state.display_bar == true) {
										this.hideBar();
									}
						}}/>
					}
			/>
		{ 	this.state.display_bar &&
			<Animated.View style={[{ height: this.state.height, opacity: this.state.opacity}]}>
				<View style={{flexDirection: 'row', justifyContent: 'flex-end', margin: 0, padding: 0}}>
				<CheckBox
				title='Info'
				containerStyle={styles.checkbox}
				checkedIcon={<Icon name='info' type='MaterialIcons' color="#019688" />}
				uncheckedIcon={<Icon name='info' type='MaterialIcons' color="grey"/>}
				checked={this.state.info}
				onPress={() => { 
					this.setState({info: !this.state.info}); 
				}}

				/>
				<CheckBox
				title='Help'
				containerStyle={styles.checkbox}
				checkedIcon={<Icon name='healing' type='MaterialIcons' color="#ff5528" />}
				uncheckedIcon={<Icon name='healing' type='MaterialIcons' color="grey"/>}
				checked={this.state.help}
				onPress={() => { 
					this.setState({help: !this.state.help}); 
				}}
				/>
				<CheckBox
				containerStyle={styles.checkbox}
				title='Social'
				checkedIcon={<Icon name="people" color="#00bcd6" />}
				uncheckedIcon={<Icon name="people" color="grey" />}
				checked={this.state.social}
				onPress={() => { 
					this.setState({social: !this.state.social}); 
				}}
				/>
				<Button
				title="Send"
				type="clear"
				buttonStyle={{alignSelf: 'flex-end', padding: 2, paddingBottom: 10, marginHorizontal: 0}}
				onPress={() => {this.submit()}}
				/>
				</View>
			</Animated.View>
			}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	checkbox: {
		borderWidth: 0,
		backgroundColor: '#ffffff',
		padding: 0
	},
	item: {
		backgroundColor: '#ffffff',
		padding: 10,
		marginVertical: 8,
		marginHorizontal: 16,
		borderColor: '#606770',
		borderWidth: 0.4,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
	},
	title: {
		fontSize: 32,
	},
	comments: {
		textAlign: 'right',
		color: '#606770',
		marginTop: 6,
		paddingBottom: 10
	}
});

export default FeedPost;