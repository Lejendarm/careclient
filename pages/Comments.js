import React from "react";
import { Animated, StyleSheet, View, Image, Text, TextInput } from "react-native";
import MaterialButtonDark from "../components/MaterialButtonDark";
import { Button, ListItem, Divider, Icon, Input, CheckBox } from 'react-native-elements';
import { translated } from "../utils/localization";
import { label_to_icon } from "../utils/label_to_icon";
import { SafeAreaView, FlatList } from 'react-native';
import ApiService from "../utils/ApiService";

const api = ApiService.instance();

class Comments extends React.Component {
	state = {
		comments: []
	};


constructor(props) {
	super(props);
}


render() {
		return (
			 <View>{
			 this.props.comments.map((l, i) => (
		      <ListItem
		        key={i}
		        leftAvatar={{ source: { uri: l.avatar_url } }}
		        subtitle={l.content}
		        bottomDivider
		      />))}
			 </View>
		);
	}
}

const styles = StyleSheet.create({
	
});

export default Comments;