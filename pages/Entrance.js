import React from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import MaterialButtonDark from "../components/MaterialButtonDark";
import { Button } from 'react-native-elements';
import { translated } from "../utils/localization";

function Entrance({navigation}) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/care.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <Image
        source={require("../assets/images/care_text.png")}
        resizeMode="contain"
        style={styles.title}
      ></Image>
      <View style={styles.containerActions}>
        <Button
          title={translated('Sign in')}
          buttonStyle={styles.materialButtonRegister}
          onPress={() => {
            navigation.navigate('Register')
          }}
        ></Button>
            <Button
          title={translated("Log in")}
          type="clear"
          buttonStyle={styles.materialButtonAuthenticate}
          onPress={() => {
            navigation.navigate('Authenticate')
          }}
        ></Button>

      </View>
      <Text style={styles.footer}>
        Application made with the National Youth Service as a dependency institution of the Ministry of Education and with the collaboration of Epitech Balkans.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  containerActions: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  image: {
    width: 240,
    height: 240,
    marginTop: 40,
    alignSelf: "center"
  },
  title: {
    width: 200,
    height: 100,
    marginTop: 10,
    alignSelf: "center"
  },
  materialButtonRegister: {
    width: 200,
    marginTop: 60,
    alignSelf: "center"
  },
  materialButtonAuthenticate: {
    width: 200,
    marginTop: 60,
    alignSelf: "center"
  },
  footer: {
    marginTop: 40,
    marginLeft: 10,
    marginRight: 10,
    color: "#878787",
    textAlign: "center",
    alignSelf: "center"
  }
});

export default Entrance;
