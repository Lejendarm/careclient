import React from "react";
import { Animated, StyleSheet, View, Image, Text, TextInput } from "react-native";
import MaterialButtonDark from "../components/MaterialButtonDark";
import { Button, ListItem, Divider, Icon, Input, CheckBox } from 'react-native-elements';
import { translated } from "../utils/localization";
import { label_to_icon } from "../utils/label_to_icon";
import { SafeAreaView, FlatList } from 'react-native';
import ApiService from "../utils/ApiService";

const api = ApiService.instance();

class CommentPost extends React.Component {
	state = {
			content: ''
	}

constructor(props) {
	super(props);
	this.state = {
		content: ''
	}
}

publishComment() {
	this.props.publishComment({content: this.state.content});
}

render() {
		return (
			<View>
			 <ListItem
		        key="post_comm"
		        leftAvatar={{ source: { uri: 'https://p7.hiclipart.com/preview/858/581/271/user-computer-icons-system-chinese-wind-title-column.jpg' } }}
		        title={
					<TextInput placeholder={"Comment this post"} 
						multiline={true}
						value={this.state.content}
  						onSubmitEditing={() => this.publishComment()}
						onChange={({ nativeEvent: {eventCount, target, text}}) => {
									this.setState({content: text});
									}} />
						}
					
		      />
			 </View>
		);
	}
}

const styles = StyleSheet.create({
	
});

export default CommentPost;