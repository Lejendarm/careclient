import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

function WorkInProgress() {
  return (
    <View>
     <Image
        source={require("../assets/images/wip.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <Text style={styles.text}>This page is not avalable yet, we will inform you when it will be ready. :)</Text>


    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  image: {
    width: 240,
    height: 240,
    marginTop: 120,
    alignSelf: "center"
  },
  text: {
    margin: 20,
    marginTop: 60,

    textAlign: "center",
    alignSelf: "center"
  }
});

export default WorkInProgress;