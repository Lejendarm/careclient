import React from "react";
import { StyleSheet, View, Icon } from "react-native";

import Entrance from "./pages/Entrance";
import Feed from "./pages/Feed";
import Register from "./pages/Register";
import Authenticate from "./pages/Authenticate";
import WorkInProgress from "./pages/WorkInProgress";

import getSettings from "./config/get-settings";
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

export default class App extends React.Component {

  render() {
    return  <NavigationContainer style={styles.container}>
                <Drawer.Navigator style={styles.screen} initialRouteName="Entrance">
                  <Drawer.Screen name="Entrance" component={Entrance} style={styles.screen}/>
                  <Drawer.Screen name="Register" component={Register} style={styles.screen}/>
                  <Drawer.Screen name="Authenticate" component={Authenticate} style={styles.screen}/>
                  <Drawer.Screen name="Feed" component={Feed}/>
                  <Drawer.Screen name="Help" component={WorkInProgress}/>
                  <Drawer.Screen name="Profile" component={WorkInProgress}/>
                  <Drawer.Screen name="Inbox" component={WorkInProgress}/>
                  <Drawer.Screen name="Emergency" component={WorkInProgress}/>
                  <Drawer.Screen name="Contacts" component={WorkInProgress}/>
                </Drawer.Navigator>
            </NavigationContainer>;
  }
}

const styles = StyleSheet.create({
   container: {
     flex: 1,
     backgroundColor: "#fff",
     alignItems: "center",
     justifyContent: "center"
   },
   screen: {
     backgroundColor: "#fff"
   } 
});