const Env = {
  development: {
    apiUrl: 'https://care-x02.appspot.com/'
  },
  staging: {
    apiUrl: "https://care-x02.appspot.com/"
  },
  prod: {
    apiUrl: "https://care-x02.appspot.com/"
  }
};

export default Env;